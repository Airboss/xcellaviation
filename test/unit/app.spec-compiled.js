'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _srcApp = require('../../src/app');

var RouterStub = (function () {
  function RouterStub() {
    _classCallCheck(this, RouterStub);
  }

  _createClass(RouterStub, [{
    key: 'configure',
    value: function configure(handler) {
      handler(this);
    }
  }, {
    key: 'map',
    value: function map(routes) {
      this.routes = routes;
    }
  }]);

  return RouterStub;
})();

describe('the App module', function () {
  var sut, mockedRouter;

  beforeEach(function () {
    mockedRouter = new RouterStub();
    sut = new _srcApp.App(mockedRouter);
    sut.configureRouter(mockedRouter, mockedRouter);
  });

  it('contains a router property', function () {
    expect(sut.router).toBeDefined();
  });

  it('configures the router title', function () {
    expect(sut.router.title).toEqual('Aurelia');
  });

  it('should have a welcome route', function () {
    expect(sut.router.routes).toContain({ route: ['', 'welcome'], moduleId: './welcome', nav: true, title: 'Welcome' });
  });

  it('should have a flickr route', function () {
    expect(sut.router.routes).toContain({ route: 'flickr', moduleId: './flickr', nav: true });
  });

  it('should have a child router route', function () {
    expect(sut.router.routes).toContain({ route: 'child-router', moduleId: './child-router', nav: true, title: 'Child Router' });
  });
});

//# sourceMappingURL=app.spec-compiled.js.map