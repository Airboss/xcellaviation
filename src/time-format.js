import moment from 'moment';

export class TimeFormatValueConverter {
  toView(value, zone)
  {
    if(value === undefined || value === "") return;

    var time = (zone === 'LCL') ? value.format('h:mm:ss A') : value.utc().format('HH:mm:ss Z');
    return  time;
  }
}
