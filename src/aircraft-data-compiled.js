"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _aureliaHttpClient = require("aurelia-http-client");

var AircraftData = (function () {
  function AircraftData() {
    _classCallCheck(this, AircraftData);

    this.list = [];
    getAircraftData();
  }

  _createClass(AircraftData, [{
    key: "getAircraftData",
    value: function getAircraftData() {
      //get Aircraft data!
      this.http.get("dist/aircraft.json").then(function (response) {
        var acList = response.content;
      });
    }
  }, {
    key: "add",
    value: function add(config) {
      var ac = new aircraft(config);
      this.list.push(ac);
    }
  }]);

  return AircraftData;
})();

exports.AircraftData = AircraftData;

//# sourceMappingURL=aircraft-data-compiled.js.map