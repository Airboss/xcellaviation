import {aircraftData} from './aircraft-data';

export class Home {

  static inject = [aircraftData];
  constructor(aircraftData) {
    this.aircraftData = aircraftData;
  }

  heading = 'Welcome to the X-Cell Aviation Website!';

}


