import {computedFrom} from 'aurelia-framework';
import * as LogManager from 'aurelia-logging';
import * as utils from 'html-utils'
import bootstrapSlider from 'bootstrap-slider'
import {MultiObserver} from 'multi-observer';
import {aircraftData} from './aircraft-data';
import $ from 'bootstrap';

export class Performance {
  static inject = [MultiObserver,aircraftData];

  heading = 'Performance';

  constructor(multiObserver, aircraftData) {
    this.version = 1;

    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.aircraft = aircraftData.availableAircraft[aircraftData.selectedAircraft];

    this._xaxis = 0;
    this._yaxis = 0;


    this.isTakeoff = true;
    this.setChartValues();

    this.leg1 = new Leg(multiObserver);

    this.legData = [
      this.leg1
    ];

    this.selectedLeg = 0;
  }

  activate(params, routeConfig) {
    if(params.p === undefined) {
      this.loadLegData();
      return;
    }

    this.loadParamLegData(params.p);
  }

  canDeactivate() {
    this.saveLegData();
  }

  addLeg() {
    this.legData.push(new Leg(this.multiObserver));
  }

  removeLeg() {
    if(this.legData.length > 1)
      this.legData.pop();

    if(this.selectedLeg > this.legData.length-1)
      this.selectedLeg = this.legData.length -1;
  }

  set xaxis(value) {
    this._xaxis = value;
    $('#xaxis').css('left', (this.xOffset + parseInt(value)) + 'px');
    this.chartDist = Math.round(this.xRngStart + (this._xaxis * this.distScalar));
  }

  get xaxis() {
    return this._xaxis;
  }

  set yaxis(value) {
    this._yaxis = value;
    $('#yaxis').css('top', (this.yOffset - (parseInt(value)/this.altScalar)) + 'px');
    this.chartAlt =  value; //Math.round(this.yRngStart + (this._yaxis * this.altScalar));
  }

  get yaxis() {
    return this._yaxis;
  }

  get chartAlt() {
    return this._chartAlt;
  }

  set chartAlt(value) {
    this._chartAlt =  value;
    this.yaxis = value;
  }

  toggleChart() {
    this.isTakeoff = !this.isTakeoff;
    this.setChartValues();
  }

  setChartValues() {
    if(this.isTakeoff) {
      this.takeoffImage = this.aircraft.takeoffDistData[0];
      this.xOffset = this.aircraft.takeoffDistData[1];
      this.xLimit = this.aircraft.takeoffDistData[2];
      this.yOffset = this.aircraft.takeoffDistData[3];
      this.yLimit = this.aircraft.takeoffDistData[4];
      this.xRngStart = this.aircraft.takeoffDistData[5];
      this.xRngEnd = this.aircraft.takeoffDistData[6];
      this.yRngStart = this.aircraft.takeoffDistData[7];
      this.yRngEnd = this.aircraft.takeoffDistData[8];
      this.distScalar = (this.xRngEnd-this.xRngStart) / this.xLimit;
      this.altScalar = (this.yRngEnd-this.yRngStart) / this.yLimit;
    }
    else {
      this.landingImage = this.aircraft.landingDistData[0];
      this.xOffset = this.aircraft.landingDistData[1];
      this.xLimit = this.aircraft.landingDistData[2];
      this.yOffset = this.aircraft.landingDistData[3];
      this.yLimit = this.aircraft.landingDistData[4];
      this.xRngStart = this.aircraft.landingDistData[5];
      this.xRngEnd = this.aircraft.landingDistData[6];
      this.yRngStart = this.aircraft.landingDistData[7];
      this.yRngEnd = this.aircraft.landingDistData[8];
      this.distScalar = (this.xRngEnd-this.xRngStart) / this.xLimit;
      this.altScalar = (this.yRngEnd-this.yRngStart) / this.yLimit;
    }
  }
  resetData() {
    if(typeof(Storage) !== undefined){
      localStorage.removeItem("xcellweb-performancelegs");
      //window.location.reload(false);
      for(var i = 0; i < this.legData.length; i++)
        this.legData[i].setDefault();
    }
  }

  emailData() {
    if(typeof(Storage) !== undefined){
      var array = [], data = "";
      for(var i = 0; i < this.legData.length; i++){
        var a = [this.legData[i].icao, this.legData[i].elev, this.legData[i].baroHg, this.legData[i].highTemp, this.legData[i].toDist, this.legData[i].toDist50, this.legData[i].ldDist, this.legData[i].ldDist50]
        array.push(a);
      }

      data = this.version + "|" + this.aircraft.reg + "|" + JSON.stringify(array);
      //data = data.replace(/["]/g, "");
      var target = "http://xcell.theflighthangar.com/#/performance?p=" + data;
      var url = "mailto:?body=<a href='" + target + "'>Performance Link</a>&subject=Performance " + this.aircraft.title;
      window.location = url;
    }
  }

  saveLegData() {
    if(typeof(Storage) !== undefined){
      var array = [], data = "";
      for(var i=0; i < this.legData.length; i++){
        var a = [this.legData[i].icao, this.legData[i].elev, this.legData[i].baroHg, this.legData[i].highTemp, this.legData[i].toDist, this.legData[i].toDist50, this.legData[i].ldDist, this.legData[i].ldDist50]
        array.push(a);
      }

      data = this.version + "|" + this.aircraftData.selectedAircraft + "|" + JSON.stringify(array);
      localStorage.setItem("xcellweb-performancelegs", data);
    }
  }

  loadLegData() {

    var data = localStorage.getItem("xcellweb-performancelegs");

    if (data === null) return;

    var ss = data.split("|");

    if (ss.length !== 3) return;

    //version check
    if (parseInt(ss[0]) !== this.version) {
      alert("Version mismatch! Data not loaded.");
      return;
    }

    this.legData = [new Leg(this.multiObserver, this.aircraft)];

    this.loadData(ss[2]);
  }

  loadParamLegData(data) {
    if(data === null) return;

    var ss = data.split("|");

    //version check
    if(parseInt(ss[0]) !== this.version) {
      alert("Version mismatch! Data not loaded.");
      return;
    }

    if(ss.length !== 3) return;

    var acIndex = -1;

    for(var i = 0; i < this.aircraftData.availableAircraft.length; i++) {
      if(this.aircraftData.availableAircraft[i].reg === ss[1]) {
        acIndex = i;
        break;
      }
    }

    if(acIndex === -1) {
      alert("Aircraft Registration not found! Data not loaded.");
      return;
    }

    this.aircraft = this.aircraftData.availableAircraft[acIndex];
    this.legData = [new Leg(this.multiObserver, this.aircraft)];

    this.setChartValues();
    this.loadData(ss[2]);
  }

  loadData(data) {
    var arrays = JSON.parse(data);
    for (var i = 0; i < arrays.length; i++) {
      var data = arrays[i];
      if(this.legData[i] == undefined) {
        this.legData[i] = new Leg(this.multiObserver, this.aircraft);
      }
      var leg = this.legData[i];

      leg.icao = data[0];
      leg.elev = data[1];
      leg.baroHg = data[2];
      leg.highTemp = data[3];
      leg.toDist = data[4];
      leg.toDist50 = data[5];
      leg.ldDist = data[6];
      leg.ldDist50 = data[7];
    }
  }

  selectLeg(index) {
    this.selectedLeg = index;
  }

  updateTODist() {
    this.legData[this.selectedLeg].toDist = this.chartDist;
  }

  updateTODist50() {
    this.legData[this.selectedLeg].toDist50 = this.chartDist;
  }

  updateLndDist() {
    this.legData[this.selectedLeg].ldDist = this.chartDist;
  }

  updateLndDist50() {
    this.legData[this.selectedLeg].ldDist50 = this.chartDist;
  }

}

export class Leg {
  stdHg = 29.92;

  constructor(multiObserver, options){
    this.multiObserver = multiObserver;
    this.setDefault(options);

    this.multiObserver.observe(
      [[this, 'elev'], [this, 'baroHg']],
      ()=>this.calcPresAlt()
    );
  }

  setDefault(options) {
    options = options || {icao: "", elev: 0, highTemp: 0, stdTemp: 15, presAlt: 0, densityAlt: 0, baroHg: 0, toDist: 0, toDist50: 0, ldDist: 0, ldDist50: 0};

    this.icao = options.icao;
    this.elev = options.elev;
    this.highTemp = options.highTemp;
    this.baroHg = options.baroHg;
    this.toDist = options.toDist;
    this.toDist50 = options.toDist50;
    this.ldDist = options.ldDist;
    this.ldDist50 = options.ldDist50;

    this.calcPresAlt();
    this.calcDensityAlt();
  }

  calcPresAlt(){
    this.presAlt = parseInt(this.elev) + ~~((this.stdHg-this.baroHg)*1000);
    this.calcStdTemp();
  }

  calcStdTemp(){
    var diff = (~~(this.presAlt/1000)) * 2;
    var fractional = ((this.presAlt%1000)+1000)%1000;

    if(fractional > 249 && fractional < 750)
      diff += 1;
    else if(fractional > 749)
      diff += 2;

    this.stdTemp = 15 - diff;

    return this.stdTemp;
  }

  calcDensityAlt() {
    return ~~(this.presAlt + (120.0 * (this.highTemp-this.stdTemp) + .5));
  }

  @computedFrom('highTemp', 'presAlt', 'stdTemp')
  get densityAlt(){
    return this.calcDensityAlt();
  }

}
