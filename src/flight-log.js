import {inject, computedFrom} from 'aurelia-framework';
import * as LogManager from 'aurelia-logging';
import moment from 'moment';
import {CourseChange} from 'course-change';
import {NavLog} from 'nav-log';
import {aircraftData} from 'aircraft-data';
import {MultiObserver} from 'multi-observer';

export class FlightLog
{
  static inject = [MultiObserver, aircraftData];
  constructor( multiObserver, aircraftData)
  {
    this.multiObserver = multiObserver;
    this.aircraft = aircraftData.availableAircraft[aircraftData.selectedAircraft];

    this.heading = 'Trip Log';
    this.myTimer;
    this.currentGMT = moment().utc().format('HH:mm:ss');
    this.currentLCL = moment().format('h:mm:ss A');
    this.takeOffTime = "";
    this.lockTakeOffTime = false;
    this.startDescent = "";
    this.routeFrom;
    this.routeTo;
    this.legAltitude = "4500";
    this.courseChanges = [new CourseChange(this.multiObserver)];
    this.courseDistTotal = 0;
    this.courseFltTimeTotal = 0;
    this.fuelEnroute = 0;
    this.fuelClimb = 0;
    this.fuelReserve = 0;
    this.fuelTotal = 0;

    this.navLogFixed = [new NavLog(this.multiObserver),new NavLog(this.multiObserver)];
    this.navLogs = [new NavLog(this.multiObserver)];

    this.multiObserver.observe(
            [[this.courseChanges[0], 'flttime'], [this.courseChanges[0], 'legdist']],
            ()=>this.calcCourseTotals()
    );

    this.multiObserver.observe(
        [
          [this, 'fuelEnroute'],
          [this, 'fuelClimb'],
          [this, 'fuelReserve']
        ],
        ()=>this.calcFuelTotals()
    );
  }

  activate(params, queryString, routeConfig)
  {
    this.myTimer = setInterval(()=>
    {
      this.updateTimer();
    }, 1000);

    var timeItems = $('input.timeEntry');

    for (var i = 0; i < timeItems.length; i++)
    {
      var timeItem = timeItems[i];
      timeItem.jStepper({mimValue: 0, MaxValue: 2400, minLength: 4});
    }
  }

  deactivate()
  {
    window.clearInterval(this.myTimer);
  }

  addCourseRow() {
    this.courseChanges.push(new CourseChange(this.multiObserver));
    this.multiObserver.observe(
            [[this.courseChanges[this.courseChanges.length-1], 'flttime'], [this.courseChanges[this.courseChanges.length-1], 'legdist']],
            ()=>this.calcCourseTotals());
  }

  removeCourseRow() {
    if(this.courseChanges.length > 1)
      this.courseChanges.pop();

    this.calcCourseTotals();
  }

  addNavLog() {
    this.navLogs.push(new NavLog(this.multiObserver));
    //this.multiObserver.observe(
    //  [[this.navLogs[this.courseChanges.length-1], 'flttime'], [this.navLogs[this.navLogs.length-1], 'legdist']],
    //  ()=>this.calcCourseTotals());
  }

  removeNavLog() {
    if(this.navLogs.length > 1)
      this.navLogs.pop();

    //this.calcCourseTotals();
  }

  get flttimeTotal() {
    return this.courseFltTimeTotal;
  }

  calcCourseTotals() {
    var seconds = 0;
    var dist = 0;
    for(var i=0; i < this.courseChanges.length; i++) {
      seconds += this.courseChanges[i].flttime;
      dist += parseInt(this.courseChanges[i].legdist);
    }

    this.courseFltTimeTotal = seconds;
    this.courseDistTotal = dist;
  }

  calcFuelTotals() {
    this.fuelTotal = parseFloat(this.fuelEnroute) + parseFloat(this.fuelClimb) + parseFloat(this.fuelReserve);
  }

  updateTimer()
  {
    this.currentGMT = moment().utc().format('HH:mm:ss');
    this.currentLCL = moment().format('h:mm:ss A');
  }

  setTakeOffTime()
  {
    if(this.lockTakeOffTime) return;

      this.takeOffTime = this.currentGMT;
      this.lockTakeOffTime = true;
  }

  toggleLockTakeOffTime() {
    this.lockTakeOffTime = ! this.lockTakeOffTime;
  }

  setLandingTime()
  {
    this.landingTime = currentGMT;
  }

  collapseToggle()
  {
    var src = this.$event.srcElement;
    if ($(src).hasClass('panel-collapsed'))
    {
      $(src).parents('.panel').find('.panel-body').slideDown();
      $(src).removeClass('panel-collapsed');
    }
    else
    {
      $(src).parents('.panel').find('.panel-body').slideUp();
      $(src).addClass('panel-collapsed');
    }
  }

  pad(num, size)
  {
    var s = String(num);
    while (s.length < (size || 2))
    {
      s = "0" + s;
    }
    return s;
  }
}

export class DateFormatValueConverter {
  toView(value, format) {
    return moment(value).format(format);
  }
}



