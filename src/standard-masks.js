import {inject, customAttribute} from 'aurelia-framework';
//import 'jquery.inputmask';
//import 'jquery.inputmask/dist/inputmask/jquery.inputmask.extensions';
//import 'jquery.inputmask/dist/inputmask/jquery.inputmask.date.extensions';

import inputmask from 'inputmask';

export class ThreeDigitsOnlyCustomAttribute {
  static inject = [Element];
	constructor(element) {
		this.element = element;
	}

  bind() {
    $(this.element).mask("999", {placeholder: "___"});
  }
}

//@customAttribute('date-mask')
//@inject(Element)
//export class DateMask {
//  constructor(element) {
//    this.element=element;
//  }
//
//  bind() {
//    $(this.element).inputmask('date');
//  }
//
//}
//export class ThreeDigitsOneDecimalOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("00.9", {placeholder: "__._", reverse: true});
//  }
//}
//
//export class SignedTwoDigitsOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("Z90", {placeholder: "-__", translation: {'Z': {pattern: /-/, optional: true}}});
//  }
//}
//
//export class BearingSpeedOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("999/900", {placeholder: "___/___"});
//  }
//}
