export class SecondsToHhmmValueConverter {
  toView(value){
    var date = new Date(null);
    date.setSeconds(value);

    return date.toISOString().substr(11, 5);
  }
}
