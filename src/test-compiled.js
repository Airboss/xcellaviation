'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _courseChange = require('course-change');

var TripLog = (function () {
  function TripLog() {
    var _this = this;

    _classCallCheck(this, TripLog);

    this.courseChanges = Array.from(new Array(1), function () {
      return new _courseChange.CourseChange(_this.calcCourseTotals.bind(_this));
    });
    this.courseDistTotal = 0;
    this.courseFltTimeTotal = 0;
  }

  _createClass(TripLog, [{
    key: 'calcCourseTotals',
    value: function calcCourseTotals() {
      var seconds = 0;
      var dist = 0;
      for (var i = 0; i < this.courseChanges.length; i++) {
        seconds += this.courseChanges[i].flttime;
        dist += parseInt(this.courseChanges[i].legdist);
      }

      this.courseFltTimeTotal = seconds;
      this.courseDistTotal = dist;
    }
  }]);

  return TripLog;
})();

exports.TripLog = TripLog;

//# sourceMappingURL=test-compiled.js.map