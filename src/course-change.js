import {computedFrom} from 'aurelia-framework';
import * as LogManager from 'aurelia-logging';

export class CourseChange {
  constructor(multiObserver)
  {
    this.multiObserver = multiObserver;

    this.tas = 0;
    this.tc = 0;
    this.winds = "0/0";
    this.wca = 0;
    this.mvar = 0;
    this.mh = 0;
    this.gs = 0;
    this.legdist = 0;
    this.flttime = 0;

    this.multiObserver.observe([[this, 'gs'], [this, 'legdist']], this.calcFltTime.bind(this));
    this.multiObserver.observe([[this, 'tc'], [this, 'wca'], [this, 'mvar']], this.calcMagHeading.bind(this));

  }

  calcMagHeading() {
    var hdg = parseInt(this.tc) + parseInt(this.wca) + parseInt(this.mvar);
    if(hdg < 0) hdg += 360;
    if(hdg > 359) hdg = hdg % 360;

    return this.mh = hdg;
  }

  calcFltTime() {
    var seconds = 0;
    if(parseFloat(this.gs) !== 0.0)
      seconds = parseInt((parseFloat(this.legdist) / parseFloat(this.gs)) * 3600);

    this.flttime = seconds;
  }
}
