import {customElement, bindable} from 'aurelia-framework';

@customElement('hdg')
export class Heading {
  @bindable value;

  onChange() {
    this.value = this.value.padZero(3);
  }
}