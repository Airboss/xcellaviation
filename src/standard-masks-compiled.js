"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _aureliaFramework = require("aurelia-framework");

var ThreeDigitsOnlyAttachedBehavior = (function () {
  function ThreeDigitsOnlyAttachedBehavior(element) {
    _classCallCheck(this, ThreeDigitsOnlyAttachedBehavior);

    this.element = element;
    $(element).mask("009", { placeholder: "___" });
  }

  _createClass(ThreeDigitsOnlyAttachedBehavior, null, [{
    key: "inject",
    value: function inject() {
      return [Element];
    }
  }]);

  return ThreeDigitsOnlyAttachedBehavior;
})();

exports.ThreeDigitsOnlyAttachedBehavior = ThreeDigitsOnlyAttachedBehavior;

//export class ThreeDigitsOneDecimalOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("00.9", {placeholder: "__._", reverse: true});
//  }
//}
//
//export class SignedTwoDigitsOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("Z90", {placeholder: "-__", translation: {'Z': {pattern: /-/, optional: true}}});
//  }
//}
//
//export class BearingSpeedOnlyAttachedBehavior {
//  static inject() {
//    return [Element];
//  }
//
//  constructor(element) {
//    this.element = element;
//    $(element).mask("999/900", {placeholder: "___/___"});
//  }
//}

//# sourceMappingURL=standard-masks-compiled.js.map