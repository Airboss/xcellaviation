"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EastWestToIntValueConverter = (function () {
  function EastWestToIntValueConverter() {
    _classCallCheck(this, EastWestToIntValueConverter);
  }

  _createClass(EastWestToIntValueConverter, [{
    key: "toView",
    value: function toView(value) {
      return value;
    }
  }, {
    key: "fromView",
    value: function fromView(value) {
      var idx = value.toUpperCase().indexOf("E");
      if (idx != -1) {
        var sval = value.substr(0, idx);
        return parseInt(-sval);
      }

      idx = value.toUpperCase().indexOf("W");
      if (idx != -1) {
        var sval = value.substr(0, idx);
        return parseInt(+sval);
      }

      return value;
    }
  }]);

  return EastWestToIntValueConverter;
})();

exports.EastWestToIntValueConverter = EastWestToIntValueConverter;

//# sourceMappingURL=east-west-to-int-compiled.js.map