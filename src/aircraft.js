
export class aircraft {
  constructor(config) {
    //aircraft
    this.reg = config.reg;
    this.title = config.title;

    //performance
    this.takeoffDistData = config.takeoffDistData; //img, xoffset, yoffset
    this.landingDistData = config.landingDistData; //img, xoffset, yoffset
    this.cgData = config.takeoffDistData; //img, xoffset, yoffset

    //weight & balance
    this.emptyWeight = config.emptyWeight;
    this.mtow = config.planeGrossWeight;
    this.emptyArm = config.emptyArm;
    this.cgRange = config.planeCgRange;
    this.engineOilArm = config.engineOilArm;
    this.forwardBaggageArm = config.forwardBaggageArm;
    this.pilotCopilotArm = config.pilotCopilotArm;
    this.rearPassengersArm = config.rearPassengersArm;
    this.rearBaggageArm = config.rearBaggageArm;
    this.fuelArm = config.fuelArm;


    //trip log
    this.stdCruise = config.stdCruise;
    this.fuelGph = config.fuelGph;
    this.stdFuelClimb = config.stdFuelClimb;
  }
}
