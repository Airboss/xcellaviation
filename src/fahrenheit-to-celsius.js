export class FahrenheitToCelsiusValueConverter {
  toView(value){
    return value;
  }

  fromView(value){
    var idx = value.toUpperCase().indexOf("F");
    if( idx != -1)
    {
      var sval = value.substr(0,idx);
      return Math.round((sval - 32) * 5 / 9);
    }

    return value;
  }
}
