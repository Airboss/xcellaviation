"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FahrenheitToCelsiusValueConverter = (function () {
  function FahrenheitToCelsiusValueConverter() {
    _classCallCheck(this, FahrenheitToCelsiusValueConverter);
  }

  _createClass(FahrenheitToCelsiusValueConverter, [{
    key: "toView",
    value: function toView(value) {
      return value;
    }
  }, {
    key: "fromView",
    value: function fromView(value) {
      var idx = value.toUpperCase().indexOf("F");
      if (idx != -1) {
        var sval = value.substr(0, idx);
        return Math.round((sval - 32) * 5 / 9);
      }

      return value;
    }
  }]);

  return FahrenheitToCelsiusValueConverter;
})();

exports.FahrenheitToCelsiusValueConverter = FahrenheitToCelsiusValueConverter;

//# sourceMappingURL=fahrenheit-to-celsius-compiled.js.map