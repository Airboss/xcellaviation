import {aircraftData} from './aircraft-data';
import {MultiObserver} from 'multi-observer';

export class WeightBalanceLeg {

  constructor(multiObserver, aircraft, options) {
    this.multiObserver = multiObserver;
    this.aircraft = aircraft;

    this.setDefault(options);
    this.doCalculations();

    this.multiObserver.observe(
      [[this, "pilotCopilotWeight"], [this, "rearPassengersWeight"], [this, "baggageWeight"], [this, "fuelLoaded"], [this, "fuelUsed"]],
      ()=>this.doCalculations()
    );
  }

  setDefault(options) {
    options = options || {
        pilotCopilotWeight: 0,
        rearPassengersWeight: 0,
        baggageWeight: 0,
        fuelLoaded: 0,
        fuelUsed: 0
      };

    this.planeEmptyWeight = this.aircraft.planeEmptyWeight;
    this.planeEmptyArm = this.aircraft.planeEmptyArm;
    this.planeEmptyMoment = this.aircraft.planeEmptyWeight * this.aircraft.planeEmptyArm;

    this.pilotCopilotWeight = options.pilotCopilotWeight;
    this.pilotCopilotArm = this.aircraft.pilotCopilotArm;
    this.pilotCopilotMoment = options.pilotCopilotWeight * this.aircraft.pilotCopilotArm;

    this.rearPassengersWeight = options.rearPassengersWeight;
    this.rearPassengersArm = this.aircraft.rearPassengersArm;
    this.rearPassengersMoment = options.rearPassengersWeight * this.aircraft.rearPassengersArm;

    this.baggageWeight = options.baggageWeight;
    this.baggageArm = this.aircraft.baggageArm;
    this.baggageMoment = options.baggageWeight * this.aircraft.baggageArm;

    this.fuelStartWeight = options.fuelLoaded * 6.0;
    this.fuelStartArm = this.aircraft.fuelArm;
    this.fuelStartMoment = options.fuelStartWeight * this.aircraft.fuelArm;

    this.fuelEndWeight = (options.fuelLoaded - options.fuelUsed) * 6.0;
    this.fuelEndArm = this.aircraft.fuelArm;
    this.fuelEndMoment = options.fuelEndWeight * this.aircraft.fuelArm;

    this.fuelCapacity = this.aircraft.fuelCapacity;

    this.fuelLoaded = options.fuelLoaded;
    this.fuelUsed = options.fuelUsed;

    this.takeoffWeight = 0;
    this.takeoffCg = 0;
    this.landingWeight = 0;
    this.landingCg = 0;

    this.grossWeightMax = this.aircraft.planeGrossWeight;
    this.cgRangeMin = this.aircraft.planeCgRange[0];
    this.cgRangeMax = this.aircraft.planeCgRange[1];
    this.cgRange = this.cgRangeMin + "-" + this.cgRangeMax;

    this.isFuelOutsideCapacity = false;
    this.isFuelUsedGreaterThanLoaded = false;
    this.isTakeoffOverGross = false;
    this.isTakeoffOutsideCg = false;
    this.isLandingOverGross = false;
    this.isLandingOutsideCg = false;
  }


  doCalculations() {
    this.pilotCopilotMoment = this.pilotCopilotWeight * this.aircraft.pilotCopilotArm;
    this.rearPassengersMoment = this.rearPassengersWeight * this.aircraft.rearPassengersArm;
    this.baggageMoment =  this.baggageWeight * this.aircraft.baggageArm;

    this.fuelStartWeight = this.fuelLoaded * 6.0;
    this.fuelStartMoment = this.fuelStartWeight * this.aircraft.fuelArm;

    this.fuelEndWeight = (this.fuelLoaded - this.fuelUsed)  * 6.0;
    this.fuelEndMoment = this.fuelEndWeight * this.aircraft.fuelArm;

    this.calcTakeoffWeight();
    this.calcLandingWeight();
    this.checkLimits();
  }

  calcTakeoffWeight() {
    var weight = parseInt(this.aircraft.planeEmptyWeight) + parseInt(this.pilotCopilotWeight) + parseInt(this.rearPassengersWeight) + parseInt(this.baggageWeight) + parseInt(this.fuelStartWeight);
    var cg = parseInt(this.planeEmptyMoment) + parseInt(this.pilotCopilotMoment) + parseInt(this.rearPassengersMoment) + parseInt(this.baggageMoment) + parseInt(this.fuelStartMoment);

    this.takeoffWeight = weight;
    this.takeoffCg = Number((cg / weight).toFixed(2));
  }

  calcLandingWeight() {
    var weight = parseInt(this.aircraft.planeEmptyWeight) + parseInt(this.pilotCopilotWeight) + parseInt(this.rearPassengersWeight) + parseInt(this.baggageWeight) + parseInt(this.fuelEndWeight);
    var cg = parseInt(this.planeEmptyMoment) + parseInt(this.pilotCopilotMoment) + parseInt(this.rearPassengersMoment) + parseInt(this.baggageMoment) + parseInt(this.fuelEndMoment);

    this.landingWeight = weight;
    this.landingCg = Number((cg / weight).toFixed(2));
  }

  checkLimits()
  {
    this.isTakeoffOverGross = this.takeoffWeight > this.grossWeightMax;
    this.isTakeoffOutsideCg = !!(this.takeoffCg > this.cgRangeMax || this.takeoffCg < this.cgRangeMin);
    this.isLandingOverGross = this.landingWeight > this.grossWeightMax;
    this.isLandingOutsideCg = !!(this.landingCg > this.cgRangeMax || this.landingCg < this.cgRangeMin);
    this.isFuelOutsideCapacity = this.fuelLoaded > this.fuelCapacity;
    this.isFuelUsedGreaterThanLoaded = (this.fuelLoaded-this.fuelUsed) < 0;
  }
}
