import {HttpClient} from 'aurelia-http-client';

export class aircraftData {
  static inject = [HttpClient];
  constructor(http) {
    this.http = http;

    this.selectedAircraft = 0;
    this.availableAircraft = [{reg: "no data", title: "no data"}];

    //get Aircraft data!
    return this.getAircraftData();
  }

  getAircraftData() {
    this.http.get("/dist/aircraft.json?nocache=" + (new Date()).getTime()).then(response => {
      this.availableAircraft = response.content;
      //alert(JSON.stringify(response.content));
    });
  }

  add(config) {
    var ac = new aircraft(config);
    this.list.push(ac);
  }
}
