export class ThreeDigitsValueConverter {
  toView(value){
    return value.padZero(3)
  }
}
