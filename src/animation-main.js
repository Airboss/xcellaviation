export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin('aurelia-computed')
    .plugin('aurelia-animator-css');

  aurelia.start().then(a => a.setRoot('app', document.body));
}
