import {CourseChange} from 'course-change';

export class TripLog
{
  constructor() {
    this.courseChanges = Array.from(new Array(1), () => new CourseChange(this.calcCourseTotals.bind(this)));
    this.courseDistTotal = 0;
    this.courseFltTimeTotal = 0;
  }

  calcCourseTotals() {
    var seconds = 0;
    var dist = 0;
    for(var i=0; i < this.courseChanges.length; i++) {
      seconds += this.courseChanges[i].flttime;
      dist += parseInt(this.courseChanges[i].legdist);
    }

    this.courseFltTimeTotal = seconds;
    this.courseDistTotal = dist;
  }

}



