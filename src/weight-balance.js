import {WeightBalanceLeg} from './weight-balance-leg';
import {aircraftData} from './aircraft-data';
import {MultiObserver} from 'multi-observer';

import $ from 'bootstrap';

export class WeightBalance {
  static inject = [MultiObserver, aircraftData];

  constructor(multiObserver, aircraftData) {
    this.version = 1;

    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.aircraft = this.aircraftData.availableAircraft[aircraftData.selectedAircraft]

    this.heading = 'Weight & Balance';
    this.displayFullMode = false;

    this.wbLegs = [new WeightBalanceLeg(this.multiObserver, this.aircraft)];
    this.multiObserver.observe(
      [[this.wbLegs[0], "takeoffCg"], [this.wbLegs[0], "landingCg"]],
      ()=>this.updateCgMarkers()
    );

    this.cgChart = this.aircraft.cgData[0];
    this.resetMarkers();
  }

  resetMarkers() {
    //this.cgMarkerCenters = [[-7,4], [-7,4], [-3,4]];
    //this.cgMarkerCenters = [[0,0], [0,0], [0,0]];
    this.cgMarkers = [[10,10], [10,10], [10,10], [10,10], [10,10], [10,10]];
  }

  activate(params, routeConfig) {
    if(params.wb === undefined) {
      this.loadLocalLegData();
      return;
    }

    this.loadParamLegData(params.wb);
  }

  deactivate() {
    this.saveLegData();
  }

  emailData() {
    if(typeof(Storage) !== undefined){
      var array = [], data = "";
      for(var i=0; i < this.wbLegs.length; i++){
        var a = [this.wbLegs[i].pilotCopilotWeight, this.wbLegs[i].rearPassengersWeight, this.wbLegs[i].baggageWeight, this.wbLegs[i].fuelLoaded, this.wbLegs[i].fuelUsed]
        array.push(a);
      }

      data = this.version + "|" + this.aircraft.reg + "|" + JSON.stringify(array);
      var target = "http://xcell.theflighthangar.com/#/weightbalance?wb=" + data;
      var url = "mailto:?body=<a href='" + target + "'>Weight %26 Balance Link</a>&subject=Weight %26 Balance " + this.aircraft.title;
      window.location = url;
    }
  }

  saveLegData() {
    if(typeof(Storage) !== undefined){
      var array = [], data = "";
      for(var i=0; i < this.wbLegs.length; i++){
        var a = [this.wbLegs[i].pilotCopilotWeight, this.wbLegs[i].rearPassengersWeight, this.wbLegs[i].baggageWeight, this.wbLegs[i].fuelLoaded, this.wbLegs[i].fuelUsed]
        array.push(a);
      }

      data = this.version + "|" + this.aircraftData.selectedAircraft + "|" + JSON.stringify(array);
      localStorage.setItem("xcellweb-weightbalancelegs", data);
    }
  }

  loadLocalLegData() {
    if (typeof(Storage) === undefined) return;

    var data = localStorage.getItem("xcellweb-weightbalancelegs");

    if(data === null) return;

    var ss = data.split("|");

    if(ss.length !== 3) return;

    //version check
    if( parseInt(ss[0]) !== this.version) {
      alert("Version mismatch! Data not loaded.");
      return;
    }

    this.loadLegData(ss[2]);
  }

  loadParamLegData(data) {
    if(data === null) return;

    var ss = data.split("|");

    //version check
    if(parseInt(ss[0]) !== this.version) {
      alert("Version mismatch! Data not loaded.");
      return;
    }

    if(ss.length !== 3) return;

    var acIndex = -1;

    for(var i = 0; i < this.aircraftData.availableAircraft.length; i++) {
      if(this.aircraftData.availableAircraft[i].reg === ss[1]) {
        acIndex = i;
        break;
      }
    }

    if(acIndex === -1) {
      alert("Aircraft Registration not found! Data not loaded.");
      return;
    }

    this.aircraft = this.aircraftData.availableAircraft[acIndex];
    this.wbLegs = [new WeightBalanceLeg(this.multiObserver, this.aircraft)];

    this.cgChart = this.aircraft.cgData[0];
    this.loadLegData(ss[2]);
  }

  loadLegData(data) {
    var arrays = JSON.parse(data);
    for (var i = 0; i < arrays.length; i++) {
      var data = arrays[i];
      if(this.wbLegs[i] == undefined) {
        this.wbLegs[i] = new WeightBalanceLeg(this.multiObserver, this.aircraft);
      }
      var leg = this.wbLegs[i];

      leg.planeEmptyWeight = this.aircraft.planeEmptyWeight;
      leg.planeEmptyArm = this.aircraft.planeEmptyArm;
      leg.planeEmptyMoment = this.aircraft.planeEmptyWeight * this.aircraft.planeEmptyArm;

      leg.pilotCopilotWeight = data[0];
      leg.pilotCopilotArm = this.aircraft.pilotCopilotArm;
      leg.pilotCopilotMoment = leg.pilotCopilotWeight * this.aircraft.pilotCopilotArm;

      leg.rearPassengersWeight = data[1];
      leg.rearPassengersArm = this.aircraft.rearPassengersArm;
      leg.rearPassengersMoment = leg.rearPassengersWeight * this.aircraft.rearPassengersArm;

      leg.baggageWeight = data[2];
      leg.baggageArm = this.aircraft.baggageArm;
      leg.baggageMoment = leg.baggageWeight * this.aircraft.baggageArm;

      leg.fuelLoaded = data[3];
      leg.fuelUsed = data[4];

      leg.fuelStartWeight =  leg.fuelLoaded* 6.0;
      leg.fuelStartArm = this.aircraft.fuelArm;
      leg.fuelStartMoment = leg.fuelStartWeight * this.aircraft.fuelStartArm;

      leg.fuelEndWeight = leg.fuelUsed * 6.0;
      leg.fuelEndArm = this.aircraft.fuelArm;
      leg.fuelEndMoment = leg.fuelEndWeight * this.aircraft.fuelEndArm;

      this.multiObserver.observe(
        [[this.wbLegs[i], "takeoffCg"], [this.wbLegs[i], "landingCg"]],
        ()=>this.updateCgMarkers()
      );
    }
  }

  addWBLeg() {
    if(this.wbLegs.length >= 3) return;

    this.wbLegs.push(new WeightBalanceLeg(this.multiObserver, this.aircraft));
    this.multiObserver.observe(
      [[this.wbLegs[this.wbLegs.length-1], "takeoffCg"], [this.wbLegs.length-1, "landingCg"]],
      ()=>this.updateCgMarkers()
    );
  }

  removeWBLeg() {
    if(this.wbLegs.length > 1)
      this.wbLegs.pop();
  }

  toggleDisplayMode() {
    this.displayFullMode = !this.displayFullMode;
  }

  get isDisplayFullMode() {
    return this.displayFullMode;
  }

  updateCgMarkers() {
    var xOffset = this.aircraft.cgData[1];
    var xwidth = this.aircraft.cgData[2];
    var yOffset = this.aircraft.cgData[3];
    var yheight = this.aircraft.cgData[4];
    var cgMin =  this.aircraft.cgData[5];
    var cgMax =  this.aircraft.cgData[6];
    var emptyweight = this.aircraft.cgData[7];
    var mtow = this.aircraft.cgData[8];

    var xscalar = xwidth / (cgMax - cgMin);
    var yscalar = yheight / (mtow - emptyweight);

    //Take off CG markers
    for(var i=0; i < this.wbLegs.length; i++){
      var x = xOffset + ((this.wbLegs[i].takeoffCg - cgMin) * xscalar) + 1;
      var y = yOffset - ((this.wbLegs[i].takeoffWeight - emptyweight)  * yscalar) + 1;
      this.cgMarkers[i][0] = y;
      this.cgMarkers[i][1] = x;
    }

    //Landing CG markers
    for(var i=0; i < this.wbLegs.length; i++){
      var x = xOffset + ((this.wbLegs[i].landingCg - cgMin) * xscalar) + 1;
      var y = yOffset - ((this.wbLegs[i].landingWeight - emptyweight)  * yscalar) - 14;
      this.cgMarkers[i+3][0] = y;
      this.cgMarkers[i+3][1] = x;
    }
  }

  resetData() {
    if(typeof(Storage) !== undefined){
      localStorage.removeItem("xcellweb-weightbalancelegs");

      this.resetMarkers();

      for(var i=0; i < this.wbLegs.length; i++)
        this.wbLegs[i].setDefault();

    }
  }
}

