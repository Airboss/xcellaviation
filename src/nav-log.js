
export class NavLog {
  constructor(multiObserver)
  {
    this.multiObserver = multiObserver;

    this.checkPoint = "";
    this.dist = 0;
    this.timeEstimate = 0;
    this.timeActual = 0;
    this.clockEstimate = "";
    this.clockActual = "";
    this.gs = 0;

    //this.multiObserver.observe([[this, 'gs'], [this, 'legdist']], this.calcFltTime.bind(this));
    //this.multiObserver.observe([[this, 'tc'], [this, 'wca'], [this, 'mvar']], this.calcMagHeading.bind(this));

  }

  calcFltTime() {
    var seconds = 0;
    if(parseFloat(this.gs) !== 0.0)
      seconds = parseInt((parseFloat(this.legdist) / parseFloat(this.gs)) * 3600);

    this.flttime = seconds;
  }
}
