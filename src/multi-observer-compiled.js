"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MultiObserver = (function () {
	function MultiObserver(observerLocator) {
		_classCallCheck(this, MultiObserver);

		this.observerLocator = observerLocator;
	}

	_createClass(MultiObserver, [{
		key: "observe",
		value: function observe(properties, callback) {
			var subscriptions = [],
			    i = properties.length,
			    object,
			    propertyName;
			while (i--) {
				object = properties[i][0];
				propertyName = properties[i][1];
				subscriptions.push(this.observerLocator.getObserver(object, propertyName).subscribe(callback));
			}

			// return dispose function
			return function () {
				while (subscriptions.length) {
					subscriptions.pop()();
				}
			};
		}
	}], [{
		key: "inject",
		value: function inject() {
			return [ObserverLocator];
		}
	}]);

	return MultiObserver;
})();

exports.MultiObserver = MultiObserver;

//# sourceMappingURL=MultiObserver-compiled.js.map