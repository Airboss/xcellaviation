'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var TimeFormatValueConverter = (function () {
  function TimeFormatValueConverter() {
    _classCallCheck(this, TimeFormatValueConverter);
  }

  _createClass(TimeFormatValueConverter, [{
    key: 'toView',
    value: function toView(value, zone) {
      if (value === undefined || value === '') return;

      var time = zone === 'LCL' ? value.format('h:mm:ss A') : value.utc().format('HH:mm:ss Z');
      return time;
    }
  }]);

  return TimeFormatValueConverter;
})();

exports.TimeFormatValueConverter = TimeFormatValueConverter;

//# sourceMappingURL=time-format-compiled.js.map