//
// All credit to Richard Shelquist @ http://wahiduddin.net/calc/density_altitude.htm
//

class DensityAltitude {
  // Global Variables for conversions
  var mb_per_in = 33.8638866667;
  var in_per_mb = (1 / mb_per_in);
  var m_per_ft = 0.304800;
  var ft_per_m = (1 / m_per_ft);
  var earth_radius_m = 6367449.1;

  constructor() {
    this.elevation = 0;
    this.temperature = 0;
    this.inHg = 0;
    this.rh = 0;
  }

  reset() {
    this.elevation = 0;
    this.temperature = 0;
    this.inHg = 0;
    this.rh = 0;
    this.awos = 0;
    this.densityAltWithRH = 0;
  }

  get elevation() {
    calcDensityAltWithRH();
    return this.elevation;
  }
  set elevation(value) {
    this.elevation = value;
  }

  get temperature() {
    calcDensityAltWithRH();
    return this.temperature;
  }
  set temperature(value) {
    this.temperature = value;
  }

  get inHg() {
    calcDensityAltWithRH();
    return this.inHg;
  }
  set inHg(value) {
    this.inHg = value;
  }

  get rh() {
    calcDensityAltWithRH();
    return this.rh;
  }
  set rh(value) {
    this.rh = value;
  }

  get AWOSDensityAlt() {
    calcDensityAltWithRH();
    return this.awos;
  }

  get DensityAltWithRH() {
    calcDensityAltWithRH();
    return this.densityAltWithRH;
  }


  static round(value, decimals) {
    var t = parseFloat(value + 'e' + decimals);
    return Number(Math.round(t) + 'e-' + decimals);
  }

  // saturation pressure of water vapor given the celsius temperature
  // Polynomial from Herman Wobus: http://wahiduddin.net
  static calcWobusVaporPressure(tc) {
    var eso = 6.1078;
    var c0 = 0.99999683;
    var c1 = -0.90826951E-02;
    var c2 = 0.78736169E-04;
    var c3 = -0.61117958E-06;
    var c4 = 0.43884187E-08;
    var c5 = -0.29883885E-10;
    var c6 = 0.21874425E-12;
    var c7 = -0.17892321E-14;
    var c8 = 0.11112018E-16;
    var c9 = -0.30994571E-19;

    var pol = c0 + tc * (c1 + tc * (c2 + tc * (c3 + tc * (c4 + tc * (c5 + tc * (c6 + tc * (c7 + tc * (c8 + tc * (c9)))))))));
    es = eso / Math.pow(pol, 8);

    return es;
  }

  static calcAirDensityKgPerM3(absPress_mb, e, tc)
  {
    var Rv = 461.4964;                //gas constant for water vapor
    var Rd = 287.0531;                // gas constant for dry air
    var tk = tc + 273.15;             //temp kelvin
    var pv = e * 100;                 // vapor pressure in pascals, multiply mb by 100 to get Pascals
    var pd = (absPress_mb - e) * 100; // dry air pressure in pascals, multiply mb by 100 to get Pascals

    return (pv / (Rv * tk)) + (pd / (Rd * tk));
  }

  static calcISAAltitudeFromDensity(d)
  {
    var Po = 101325;  // sea level standard pressure, Pa
    var To = 288.15;  // sea level standard temperature, deg K  ( 15 deg C)
    var g = 9.80665;  // gravitational constant, m/sec2
    var L = 6.5;      // temperature lapse rate, deg K/km
    var R = 8.314320; // gas constant, J/ mol*deg K
    var M = 28.9644;  // molecular weight of dry air, gm/mol
    var D = d * 1000; // convert to grams form kg

    var p2 = ((L * R) / (g * M - L * R) ) * Math.log((R * To * D) / (M * Po));
    var H = -(To / L) * (Math.exp(p2) - 1);

    return  H * 1000; //convert to m from km
  }

  static calcZ(h) // Calculate the geometric (Z) altitude (meters), given the geo-potential (H) altitude (meters)
  {
    var r = this.earth_radius_m;
    return ((r * h) / (r - h));
  }

  static calcH(z) // Calculate the geo-potential (H) altitude (meters), given the geometric (Z) altitude (meters)
  {
    var r = this.earth_radius_m;
    return ((r * z) / (r + z));
  }

  static calcAltSettingMbToActualPressAlt(mb, h) // Calculate the actual pressure
  {
    var k1 = 0.190263;
    var k2 = 8.417286E-5;
    return Math.pow((Math.pow(mb, k1) - (k2 * h)), (1 / k1));
  }

  calcDensityAltWithRH() {
    var tc = this.temperature; // input is celsius
    var tf = ((9 / 5) * tc) + 32;

    // Process the input values
    var zm = this.elevation * this.m_per_ft; // geometric elevation converted to meters
    var altSettingMb = this.inHg * this.mb_per_in; // inHg converted to millibars
    var rh = this.rh; //relative humidity

    var absPressAlt = calcH(zm); // pressure altitude - geo-potential

    var e_mb = calcWobusVaporPressure(tc) * rh / 100; // geo-potential altitude
    var actPress_mb = calcAltSettingMbToActualPressAlt(altSettingMb, absPressAlt);  // air density in millibars
    var actPress_InHg = actPress_mb * this.in_per_mb; // adjusted pressure converted back to inHg

    var density = calcAirDensityKgPerM3(actPress_mb, e_mb, tc);
    var densityAlt_m = calcISAAltitudeFromDensity(density); // geometric altitude Z (m) from geopotential altitude (m) H
    var densityAlt_m = calcZ(densityAlt_m); // Convert Units for output
    var densityAlt_ft = densityAlt_m * this.ft_per_m; // computed density altitude

    // round off density altitude
    this.densityAltWithRH = round(densityAlt_ft, 0);

    // AWOS density altitude
    var nws = 145442.16 * (1 - Math.pow(((17.326 * actPress_InHg) / (tf + 459.67)), 0.235));
    this.awos = round(nws / 100) * 100; // Print the results
  }
}
