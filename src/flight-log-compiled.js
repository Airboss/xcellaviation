'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _aureliaFramework = require('aurelia-framework');

var _aureliaLogging = require('aurelia-logging');

var LogManager = _interopRequireWildcard(_aureliaLogging);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _courseChange = require('course-change');

var TripLog = (function () {
  function TripLog() {
    _classCallCheck(this, TripLog);

    this.heading = 'Trip Log';
    this.myTimer;
    this.currentTime = '';
    this.takeOffTime = '';
    this.currTimeZone = 'LCL';
    this.routeFrom;
    this.routeTo;
    this.legAltitude = '4500';
    var cc = new _courseChange.CourseChange();
    cc.setChangeCallback(this.calcCourseTotals.bind(this));
    this.courseChanges = [cc];
    this.courseDistTotal = 0;
    this.courseFltTimeTotal = 0;
  }

  _createClass(TripLog, [{
    key: 'activate',
    value: function activate(params, queryString, routeConfig) {
      var _this = this;

      this.myTimer = setInterval(function () {
        _this.updateTimer();
      }, 1000);

      var timeItems = $('input.timeEntry');

      for (var i = 0; i < timeItems.length; i++) {
        var timeItem = timeItems[i];
        timeItem.jStepper({ mimValue: 0, MaxValue: 2400, minLength: 4 });
      }
    }
  }, {
    key: 'deactivate',
    value: function deactivate() {
      window.clearInterval(this.myTimer);
    }
  }, {
    key: 'addCourseRow',
    value: function addCourseRow() {
      this.courseChanges.push(new _courseChange.CourseChange(this.calcCourseTotals.bind(this)));
    }
  }, {
    key: 'removeCourseRow',
    value: function removeCourseRow() {
      if (this.courseChanges.length > 1) this.courseChanges.pop();
    }
  }, {
    key: 'flttimeTotal',
    get: function () {
      return this.courseFltTimeTotal;
    }
  }, {
    key: 'calcCourseTotals',
    value: function calcCourseTotals() {
      var seconds = 0;
      var dist = 0;
      for (var i = 0; i < this.courseChanges.length; i++) {
        seconds += this.courseChanges[i].flttime;
        dist += parseInt(this.courseChanges[i].legdist);
      }

      this.courseFltTimeTotal = seconds;
      this.courseDistTotal = dist;
    }
  }, {
    key: 'updateTimer',
    value: function updateTimer() {
      this.currentTime = (0, _moment2['default'])();
    }
  }, {
    key: 'changeTimezone',
    value: function changeTimezone() {
      this.currTimeZone = this.currTimeZone === 'LCL' ? 'GMT' : 'LCL';
    }
  }, {
    key: 'setTakeOffTime',
    value: function setTakeOffTime() {
      this.takeOffTime = this.currentTime;
    }
  }, {
    key: 'setLandingTime',
    value: function setLandingTime() {
      this.landingTime = this.currentTime;
    }
  }, {
    key: 'collapseToggle',
    value: function collapseToggle() {
      var src = this.$event.srcElement;
      if ($(src).hasClass('panel-collapsed')) {
        $(src).parents('.panel').find('.panel-body').slideDown();
        $(src).removeClass('panel-collapsed');
      } else {
        $(src).parents('.panel').find('.panel-body').slideUp();
        $(src).addClass('panel-collapsed');
      }
    }
  }, {
    key: 'pad',
    value: function pad(num, size) {
      var s = String(num);
      while (s.length < (size || 2)) {
        s = '0' + s;
      }
      return s;
    }
  }]);

  return TripLog;
})();

exports.TripLog = TripLog;

var DateFormatValueConverter = (function () {
  function DateFormatValueConverter() {
    _classCallCheck(this, DateFormatValueConverter);
  }

  _createClass(DateFormatValueConverter, [{
    key: 'toView',
    value: function toView(value, format) {
      return (0, _moment2['default'])(value).format(format);
    }
  }]);

  return DateFormatValueConverter;
})();

exports.DateFormatValueConverter = DateFormatValueConverter;

//# sourceMappingURL=trip-log-compiled.js.map