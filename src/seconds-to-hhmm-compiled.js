"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SecondsToHhmmValueConverter = (function () {
  function SecondsToHhmmValueConverter() {
    _classCallCheck(this, SecondsToHhmmValueConverter);
  }

  _createClass(SecondsToHhmmValueConverter, [{
    key: "toView",
    value: function toView(value) {
      var date = new Date(null);
      date.setSeconds(value);

      return date.toISOString().substr(11, 5);
    }
  }]);

  return SecondsToHhmmValueConverter;
})();

exports.SecondsToHhmmValueConverter = SecondsToHhmmValueConverter;

//# sourceMappingURL=seconds-to-hhmm-compiled.js.map