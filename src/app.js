import 'bootstrap';
import 'bootstrap/css/bootstrap.css!';
import {aircraftData} from './aircraft-data';

export class App {
  static inject = [aircraftData];
  constructor(aircraftData) {
    this.aircraftData = aircraftData;
  }

  configureRouter(config, router){
    config.title = 'X-Cell';
    config.map([
      { route: ['','home'],  moduleId: './home',      nav: false, title:'Home' },
      { route: 'weightbalance',  moduleId: './weight-balance', nav: true, title:'Weight & Balance' },
      { route: 'performance',  moduleId: './performance', nav: true, title:'Performance' },
      { route: 'flightlog',  moduleId: './flight-log', nav: false, title:'Flight Log' }
    ]);

    this.router = router;
  }
}
